Hands On SP2023_Hendra Budi Pratama

- API ini dibuat dengan menggunakan bahasa pemrograman PHP dan Framework CodeIgniter 3
- Menggunakan library CodeIgniter Rest Server
- Membutuhkan koneksi internet dalam menjalankannya karena ada proses get data dari resource API lain di internet

HANDS-ON TEST: JOIN API :

1. API ini dapat diakses tanpa menggunakan HTTP Request Headers
2. Untuk mengaksesnya , dapat diakses pada link URL {base_url}/rscovid dengan metode GET

API akan menghasilkan data dalam format JSON, yang berisi:
-Nama rumah sakit
-Jenis rumah sakit
-Alamat rumah sakit
-Kelurahan
-Kecamatan
-Kota/Kab
-Kode pos
-Nomor Telepon
-Nomor Fax
-Website
-Email

HANDS-ON TEST: FILTER API :

1. API ini dapat diakses tanpa menggunakan HTTP Request Headers
2. Untuk mengaksesnya , dapat diakses pada link URL {base_url}/rscovid dengan metode GET
3. untuk filter kelurahan , dapat menambahkan variabel kelurahan pada URL
   contoh : {base_url}/rscovid?kelurahan=kalideres
4. untuk filter kecamatan , dapat menambahkan variabel kecamatan pada URL
   contoh : {base_url}/rscovid?kecamatan=kalideres
5. untuk filter kota , dapat menambahkan variabel kota pada URL
   contoh : {base_url}/rscovid?kota=Jakarta Barat
