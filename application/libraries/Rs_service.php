<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rs_service
{
    public $response = array();
    public function __construct()
    {
        $this->url_rs = "https://data.jakarta.go.id/read-resource/get-json/rsdkijakarta-2017-10/8e179e38-c1a4-4273-872e-361d90b68434";
        $this->url_rs_covid = "https://data.jakarta.go.id/read-resource/get-json/daftar-rumah-sakit-rujukan-penanggulangan-covid-19/65d650ae-31c8-4353-a72b-3312fd0cc187";
    }

    public function getRs()
    {
        $response = $this->hitApi($this->url_rs);
        return json_decode($response);
    }

    public function getRsCovid()
    {
        $response = $this->hitApi($this->url_rs_covid);
        return json_decode($response);
    }

    public function hitApi($uri)
    {
        $headers = array(
            "Accept: application/json",
        );
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        return $response;
    }
}
