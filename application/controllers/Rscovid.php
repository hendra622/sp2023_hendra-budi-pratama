<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Rscovid extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Rs_service");
    }

    public function index_get()
    {
        $rs_covid = $this->rs_service->getRsCovid();
        $rs = $this->rs_service->getRs();

        $kelurahan = $this->input->get("kelurahan", TRUE);
        $kecamatan = $this->input->get("kecamatan", TRUE);
        $kota = $this->input->get("kota", TRUE);
        $filters = array(
            "kelurahan" => $kelurahan,
            "kecamatan" => $kecamatan,
            "kota" => $kota
        );

        $filteredRsCovid = $this->filterRsCovid($filters, $rs_covid);

        $mergedRsCovid = $this->mergeRsCovid($filteredRsCovid, $rs);

        $this->set_response($mergedRsCovid, REST_Controller::HTTP_OK);
    }

    function filterRsCovid($filters, $rs_covid)
    {
        if ($filters['kelurahan'] || $filters['kecamatan'] || $filters['kota']) {
            $new = array_values(array_filter($rs_covid, function ($var) use ($filters) {
                return (strtolower($var->kelurahan) == strtolower($filters['kelurahan']) || strtolower($var->kecamatan) == strtolower($filters['kecamatan']) || strtolower($var->kota_madya) == strtolower($filters['kota']));
            }));
        } else {
            return $rs_covid;
        }

        return $new;
    }

    function mergeRsCovid($rs_covid, $rs)
    {
        $rs_covid_baru = [];

        foreach ($rs_covid as $cov) {
            $found = false;
            foreach ($rs as $r) {
                $nama_rs_covid = str_replace("RSUD", "Rumah Sakit Umum Daerah", $cov->nama_rumah_sakit);
                $nama_rs_covid = str_replace("RSKD", "Rumah Sakit Khusus Jiwa", $nama_rs_covid);
                $nama_rs_covid = str_replace("RSAU", "Rumah Sakit Umum Pusat Angkatan Udara", $nama_rs_covid);
                $nama_rs_covid = str_replace("RS", "Rumah Sakit Umum", $nama_rs_covid);
                $nama_rs = $r->jenis_rumah_sakit . " " . $r->nama_rumah_sakit;
                $nama_rs = str_replace("Rumah Sakit Umum Kecamatan Kelas D", "Rumah Sakit Umum Daerah", $nama_rs);

                $sim = similar_text(strtolower($nama_rs_covid), strtolower($nama_rs), $perc);
                $sim2 = similar_text(strtolower($cov->alamat), strtolower($r->alamat_rumah_sakit), $perc2);
                $sim3 = similar_text(strtolower(str_replace(" ", "", $cov->kelurahan)), strtolower(str_replace(" ", "", $r->kelurahan)), $perc3);
                $sim4 = similar_text(strtolower($cov->kecamatan), strtolower($r->kecamatan), $perc4);

                if ($perc >= 81 && $perc2 >= 68) {
                    $p = array();
                    $p['nama_rumah_sakit'] = $cov->nama_rumah_sakit;
                    $p['jenis_rumah_sakit'] = $r->jenis_rumah_sakit;
                    $p['alamat_rumah_sakit'] = $r->alamat_rumah_sakit;
                    $p['kelurahan_rumah_sakit'] = $r->kelurahan;
                    $p['kecamatan_rumah_sakit'] = $r->kecamatan;
                    $p['kota_rumah_sakit'] = $cov->kota_madya;
                    $p['kodepos_rumah_sakit'] = $r->kode_pos;
                    $p['telepon_rumah_sakit'] = $r->nomor_telepon;
                    $p['fax_rumah_sakit'] = $r->nomor_fax;
                    $p['website_rumah_sakit'] = $r->website;
                    $p['email_rumah_sakit'] = $r->email;

                    $rs_covid_baru[] = $p;
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $p = array();
                $p['nama_rumah_sakit'] = $cov->nama_rumah_sakit;
                $p['jenis_rumah_sakit'] = null;
                $p['alamat_rumah_sakit'] = $cov->alamat;
                $p['kelurahan_rumah_sakit'] = $cov->kelurahan;
                $p['kecamatan_rumah_sakit'] = $cov->kecamatan;
                $p['kota_rumah_sakit'] = $cov->kota_madya;
                $p['kodepos_rumah_sakit'] = null;
                $p['telepon_rumah_sakit'] = null;
                $p['fax_rumah_sakit'] = null;
                $p['website_rumah_sakit'] = null;
                $p['email_rumah_sakit'] = null;
                $rs_covid_baru[] = $p;
            }
        }

        return $rs_covid_baru;
    }
}
