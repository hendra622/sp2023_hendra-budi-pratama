<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puskesmas_model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function list_puskesmas(){
        $this->db->from('m_puskesmas');
        $this->db->join('m_kecamatan', 'm_puskesmas.id_kecamatan = m_kecamatan.id_kecamatan', 'left');
        $this->db->join('m_kota', 'm_kecamatan.id_kota = m_kota.id_kota', 'left');
        $this->db->join('m_provinsi', 'm_kota.id_provinsi = m_provinsi.id_provinsi', 'left');
        $query = $this->db->get();
        return $query->result();
    }

}